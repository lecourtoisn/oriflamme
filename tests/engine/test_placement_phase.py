import pytest

from src.engine.enums import Position
from src.engine.exceptions import InvalidUserInputError, NotPlayerTurnError, PhaseNotStartedError, PhaseOverError, \
    NeedUserInputError, MissingPlayerOfPhaseUserInputError
from src.engine.phase import PlacementPhase
from src.engine.user_input import PlacementUserInput


@pytest.fixture
def placement_phase(events, board, player_1, player_2, player_3):
    players = [player_1, player_2, player_3]
    return PlacementPhase(events, players, board)


def test_placement_phase(placement_phase, board, player_1, player_2, player_3):
    players = [player_1, player_2, player_3]
    assert placement_phase.player_turn == player_1

    with pytest.raises(NeedUserInputError):
        placement_phase.start()
    assert placement_phase.player_turn == player_1

    iter_players = iter(players.copy())
    expected_stack = []
    while not placement_phase.is_over:
        player = next(iter_players)
        assert placement_phase.player_turn == player

        card_to_be_played = next(iter(player.hand))
        try:
            placement_phase.proceed(player, PlacementUserInput(card_to_be_played.name, Position.RIGHT))
        except PhaseOverError:
            assert placement_phase.is_over
        except NeedUserInputError:
            assert placement_phase.player_turn != player
        else:
            assert not placement_phase.is_over

        assert player not in placement_phase.remaining_players
        assert placement_phase.player_turn != player
        assert card_to_be_played in board.cards

        expected_stack.append([card_to_be_played])

    assert board.stacks == expected_stack
    assert placement_phase.is_over


def test_placement_phase_wrong_input(placement_phase, player_1):
    with pytest.raises(NeedUserInputError):
        placement_phase.start()
    assert placement_phase.player_turn == player_1

    with pytest.raises(MissingPlayerOfPhaseUserInputError):
        placement_phase.proceed(None, "I want to play a soldier, can I ? By the way I'm Jack. Place it on the left plz")

    with pytest.raises(InvalidUserInputError):
        # noinspection PyTypeChecker
        placement_phase.proceed(
            player_1, "I want to play a soldier, can I ? By the way I'm Jack. Place it on the left plz")


def test_wrong_player(placement_phase, player_1, player_2, p2_cards):
    with pytest.raises(NeedUserInputError):
        placement_phase.start()
    assert placement_phase.player_turn == player_1
    with pytest.raises(NotPlayerTurnError):
        placement_phase.proceed(player_2, PlacementUserInput(p2_cards(), Position.LEFT))


def test_placement_on(placement_phase, player_1, player_2, p1_cards, board):
    with pytest.raises(NeedUserInputError):
        placement_phase.start()
    assert placement_phase.player_turn == player_1
    base_card = p1_cards()
    top_card = p1_cards()
    board.place(base_card, Position.LEFT)
    with pytest.raises(NeedUserInputError):
        placement_phase.proceed(player_1, PlacementUserInput(top_card.name, Position.ON, base_card))
    assert placement_phase.player_turn == player_2

    assert board.stacks == [[base_card, top_card]]


def test_not_started_phase(placement_phase, player_1, player_2, p1_cards):
    assert not placement_phase.is_started
    with pytest.raises(PhaseNotStartedError):
        placement_phase.proceed(player_1, PlacementUserInput(p1_cards().name, Position.RIGHT))

    with pytest.raises(NeedUserInputError):
        placement_phase.start()

    assert placement_phase.is_started
    with pytest.raises(NeedUserInputError):
        placement_phase.proceed(player_1, PlacementUserInput(p1_cards().name, Position.RIGHT))
    assert placement_phase.player_turn == player_2
