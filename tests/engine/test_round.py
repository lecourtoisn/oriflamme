import pytest

from src.engine.enums import CardName, Position, RevealChoice
from src.engine.exceptions import NotPlayerTurnError, RoundOverError, InvalidUserInputError
from src.engine.phase import PlacementPhase, ResolutionPhase
from src.engine.round import Round
from src.engine.user_input import PlacementUserInput, RevealUserInput, ChoosePlayerUserInput


def test_round(events, board, player_1, player_2, player_3, p1_cards, p2_cards, p3_cards):
    game_round = Round(events, [player_1, player_2, player_3], board)

    assert isinstance(game_round.phase, PlacementPhase)

    assert not game_round.phase.is_started
    assert not game_round.phase.is_over

    game_round.proceed()

    assert game_round.phase.is_started
    assert game_round.phase.player_turn == player_1

    game_round.proceed(player_1, PlacementUserInput((p1_card := p1_cards(CardName.HERITIER)).name, Position.RIGHT))
    with pytest.raises(NotPlayerTurnError):
        game_round.proceed(player_1, PlacementUserInput(p1_cards(CardName.SEIGNEUR).name, Position.RIGHT))
    game_round.proceed(player_2, PlacementUserInput((p2_card := p2_cards(CardName.ESPION)).name, Position.RIGHT))
    game_round.proceed(player_3, PlacementUserInput((p3_card := p3_cards(CardName.SEIGNEUR)).name, Position.LEFT))

    assert board.stacks[0][0] == p3_card
    assert board.stacks[1][0] == p1_card
    assert board.stacks[2][0] == p2_card

    assert isinstance(game_round.phase, ResolutionPhase)

    assert game_round.phase.is_started

    with pytest.raises(NotPlayerTurnError):
        game_round.proceed(player_2, RevealUserInput(RevealChoice.REVEAL))

    assert player_3.influence == 1
    game_round.proceed(player_3, RevealUserInput(RevealChoice.REVEAL))
    assert player_3.influence == 2

    assert player_1.influence == 1
    game_round.proceed(player_1, RevealUserInput(RevealChoice.REVEAL))
    assert player_1.influence == 3

    assert player_2.influence == 1
    game_round.proceed(player_2, RevealUserInput(RevealChoice.REVEAL))
    with pytest.raises(InvalidUserInputError):
        game_round.proceed(player_2, ChoosePlayerUserInput(player_3))
    with pytest.raises(RoundOverError):
        game_round.proceed(player_2, ChoosePlayerUserInput(player_1))
    assert player_1.influence == 2
    assert player_2.influence == 2
