import pytest

from src.engine.board import Board
from src.engine.enums import Color, CardName, Position
from src.engine.exceptions import NumberOfCardsAskedTooHigh, CardNotInPlayerHandError
from src.engine.player import Player


@pytest.fixture
def player_1(events):
    return Player(events, Color.BLUE, number_cards=10)


@pytest.fixture
def player_2(events):
    return Player(events, Color.RED, number_cards=10)


@pytest.fixture
def player_3(events):
    return Player(events, Color.YELLOW, number_cards=10)


def test_player_hand(events):
    number_cards = 10
    player = Player(events, Color.BLUE, number_cards=number_cards)
    assert player.color == Color.BLUE
    assert player.influence == 1

    player_cards_names = {card.name for card in player.hand}
    assert len(player_cards_names) == number_cards
    # noinspection PyTypeChecker
    assert player_cards_names == set(CardName)

    other_player = Player(events, Color.BLUE)
    assert len(other_player.hand) == 7


def test_player_too_many_cards(events):
    with pytest.raises(NumberOfCardsAskedTooHigh):
        Player(events, Color.RED, number_cards=15)


def test_player_play_card_name(player_1: Player, board: Board):
    card_to_be_played = next(iter(player_1.hand))

    assert card_to_be_played in player_1.hand
    player_1.play_card_name(board, card_to_be_played.name, Position.LEFT)
    assert board.cards == {card_to_be_played}
    assert card_to_be_played not in player_1.hand

    other_card_to_be_played = next(iter(player_1.hand))

    assert other_card_to_be_played != card_to_be_played
    assert other_card_to_be_played in player_1.hand
    player_1.play_card_name(board, other_card_to_be_played.name, Position.ON, card_to_be_played)
    assert board.cards == {card_to_be_played, other_card_to_be_played}
    assert board.stacks == [[card_to_be_played, other_card_to_be_played]]
    assert other_card_to_be_played not in player_1.hand


def test_player_play_card_he_does_not_have(board: Board, events):
    player = Player(events, Color.BLACK, number_cards=9)

    board_status = board.stacks

    # noinspection PyTypeChecker
    card_name_he_does_not_have = next(iter(set(CardName) - {card.name for card in player.hand}))
    with pytest.raises(CardNotInPlayerHandError):
        player.play_card_name(board, card_name_he_does_not_have, Position.LEFT)

    assert board.stacks == board_status

    card_to_be_played = next(iter(player.hand))
    player.play_card_name(board, card_to_be_played.name, Position.LEFT)
    board_status = board.stacks

    card_already_played = card_to_be_played
    with pytest.raises(CardNotInPlayerHandError):
        player.play_card_name(board, card_already_played.name, Position.LEFT)

    assert board.stacks == board_status
