import pytest

from src.engine.enums import CardName, Position, RevealChoice
from src.engine.exceptions import NotPlayerTurnError, CardNotInPlayerHandError, GameOverError
from src.engine.game import Game
from src.engine.user_input import PlacementUserInput, RevealUserInput


@pytest.fixture
def events():
    return []


def test_players_in_rounds():
    game = Game(players_number=3)
    player_1, player_2, player_3 = game.players

    assert game.rounds[0].placement_phase.remaining_players == [player_1, player_2, player_3]
    assert game.rounds[1].placement_phase.remaining_players == [player_2, player_3, player_1]
    assert game.rounds[2].placement_phase.remaining_players == [player_3, player_1, player_2]
    assert game.rounds[3].placement_phase.remaining_players == [player_1, player_2, player_3]
    assert game.rounds[4].placement_phase.remaining_players == [player_2, player_3, player_1]
    assert game.rounds[5].placement_phase.remaining_players == [player_3, player_1, player_2]


def test_game_init(p_cards):
    game = Game(players_number=3, rounds_number=3, players_cards_number=10)
    for player in game.players:
        assert len(player.hand) == 10

    player_1, player_2, player_3 = game.players
    p1_cards, p2_cards, p3_cards = p_cards(player_1), p_cards(player_2), p_cards(player_3)

    assert len(game.players) == 3

    assert not game.is_started

    game.start()

    assert game.is_started
    assert game.round_counter == 0

    game.proceed(player_1, PlacementUserInput((p1_card := p1_cards(CardName.HERITIER)).name, Position.RIGHT))
    with pytest.raises(NotPlayerTurnError):
        game.proceed(player_1, PlacementUserInput(p1_cards(CardName.SEIGNEUR).name, Position.RIGHT))
    game.proceed(player_2, PlacementUserInput((p2_card := p2_cards(CardName.HERITIER)).name, Position.RIGHT))
    game.proceed(player_3, PlacementUserInput((p3_card := p3_cards(CardName.HERITIER)).name, Position.LEFT))

    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))

    assert game.round_counter == 1
    assert game.active_round.phase.is_started
    with pytest.raises(CardNotInPlayerHandError):
        game.proceed(player_2, PlacementUserInput(CardName.HERITIER, Position.RIGHT))
    with pytest.raises(NotPlayerTurnError):
        game.proceed(player_1, PlacementUserInput(CardName.SEIGNEUR, Position.RIGHT))

    game.proceed(player_2, PlacementUserInput(CardName.SEIGNEUR, Position.RIGHT))
    game.proceed(player_3, PlacementUserInput(CardName.SEIGNEUR, Position.RIGHT))
    game.proceed(player_1, PlacementUserInput(CardName.SEIGNEUR, Position.RIGHT))

    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    with pytest.raises(NotPlayerTurnError):
        game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))

    assert game.round_counter == 2
    assert game.active_round.phase.is_started

    game.proceed(player_3, PlacementUserInput(CardName.ESPION, Position.RIGHT))
    game.proceed(player_1, PlacementUserInput(CardName.ESPION, Position.RIGHT))
    game.proceed(player_2, PlacementUserInput(CardName.ESPION, Position.RIGHT))

    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_3, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    game.proceed(player_1, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    assert not game.is_over
    game.proceed(player_2, RevealUserInput(RevealChoice.DO_NOT_REVEAL))
    assert game.is_over

    with pytest.raises(GameOverError):
        game.proceed()
