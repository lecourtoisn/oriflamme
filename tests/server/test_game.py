import pytest

from src.engine.board import Position
from src.engine.exceptions import OriflammeError
from src.engine.game import Game as EngineGame
from src.engine.user_input import ChoosePlayerUserInput, ChooseCardUserInput, ChooseMovementUserInput
from src.server.chat import MAX_MESSAGE_LENGTH
from src.server.events import TerminalEventError, player_not_in_game_event, game_has_not_started_event, \
    not_a_card_name_event, not_a_resolution_choice_name_event, no_such_player_event, \
    not_a_position_name_event, no_such_card_event
from src.server.game import Game
from tests.server.test_lobby import reset_sockets


def engine_game_player_turn(engine_game: EngineGame):
    return engine_game.active_round.phase.player_turn


@pytest.fixture
def lobby_with_game(lobby_with_players):
    lobby = lobby_with_players
    p1, *_ = lobby.logged_player
    sockets = [p.ws_wrapper for p in lobby.logged_player]
    lobby.create_game(p1, "The Office")
    the_office = lobby.games[-1]
    for player in lobby.logged_player[1:4]:
        lobby.join_game(player, the_office.name)
    Game.PLAYERS_CARD_NUMBER = 10
    lobby.start_game(p1, the_office.name)
    Game.PLAYERS_CARD_NUMBER = 7
    reset_sockets(sockets)
    return lobby


@pytest.fixture
def lobby_with_game_resolution_phase(lobby_with_game):
    lobby = lobby_with_game
    game = lobby.games[0]
    engine_players = {game.engine_player(player): player for player in game.players}
    for _ in range(len(game.players)):
        engine_player = engine_game_player_turn(game.engine_game)
        player = engine_players[engine_player]
        game.play_card(player, list(engine_player.hand)[0].name.name, "RIGHT")
    return lobby


def test_as_dict(lobby_with_players):
    lobby = lobby_with_players
    p1, p2, *_ = lobby.players
    lobby.create_game(p1, "The Office")
    lobby.join_game(p2, "The Office")
    game = lobby.games[0]
    assert game.as_dict() == {
        "name": "The Office",
        "leaderId": p1.id,
        'maxPlayers': 5,
        'minPlayers': 3,
        "players": [player.id for player in game.players]
    }


def test_play_card(lobby_with_game):
    lobby = lobby_with_game
    not_in_game_player = lobby.players[-1]
    sockets = [player.ws_wrapper for player in lobby.logged_player]
    game = lobby.games[0]
    active_player = [p for p in game.players if game.engine_player(p) == engine_game_player_turn(game.engine_game)][0]

    other_player = [p for p in game.players if p is not active_player][0]

    # Player not in game
    with pytest.raises(TerminalEventError) as err:
        game.play_card(not_in_game_player, "ASSASSINAT", "LEFT")
    assert err.value.event == player_not_in_game_event(game, not_in_game_player)
    reset_sockets(sockets)

    # Game hasn't started
    engine_game = game.engine_game
    game._engine_game = None
    with pytest.raises(TerminalEventError) as err:
        game.play_card(active_player, "ASSASSINAT", "LEFT")
    assert err.value.event == game_has_not_started_event(game)
    game._engine_game = engine_game
    reset_sockets(sockets)

    # No such card
    with pytest.raises(TerminalEventError) as err:
        game.play_card(active_player, "Kuribo", "LEFT")
    assert err.value.event == not_a_card_name_event("Kuribo")
    reset_sockets(sockets)

    # The position isn't left or right
    with pytest.raises(TerminalEventError) as err:
        game.play_card(active_player, "SOLDAT", "IN MODE ATTACK")
    assert err.value.event == not_a_position_name_event("IN MODE ATTACK")
    reset_sockets(sockets)

    # The engine raised an error (whatever the error)
    card = list(game.engine_player(active_player).hand)[0]
    with pytest.raises(OriflammeError):
        game.play_card(active_player, card.name.name, Position.ON.name)
    reset_sockets(sockets)

    other_p_card = list(game.engine_player(other_player).hand)[0]
    with pytest.raises(OriflammeError):
        game.play_card(other_player, other_p_card.name.name, Position.LEFT.name)
    reset_sockets(sockets)

    # Worked fine
    card1, card2 = list(game.engine_player(active_player).hand)[:2]
    game.play_card(active_player, card1.name.name, Position.LEFT.name)

    # Asserting that the problem is not that the cover_card is not recognized
    with pytest.raises(TerminalEventError):
        game.play_card(active_player, card1.name.name, card2.name.name)
    assert err.value.event == not_a_position_name_event("IN MODE ATTACK")
    reset_sockets(sockets)


def test_choose_resolution(lobby_with_game_resolution_phase):
    lobby = lobby_with_game_resolution_phase
    game = lobby.games[0]
    active_player = [p for p in game.players if game.engine_player(p) == engine_game_player_turn(game.engine_game)][0]
    not_in_game_player = lobby.players[-1]

    # Invalid choice
    with pytest.raises(TerminalEventError) as err:
        game.choose_resolution(active_player, "EAT_THE_CARD")
    assert err.value.event == not_a_resolution_choice_name_event("EAT_THE_CARD")

    # Player not in game
    with pytest.raises(TerminalEventError) as err:
        game.choose_resolution(not_in_game_player, "REVEAL")
    assert err.value.event == player_not_in_game_event(game, not_in_game_player)

    # Game hasn't started
    engine_game = game.engine_game
    game._engine_game = None
    with pytest.raises(TerminalEventError) as err:
        game.choose_resolution(active_player, "REVEAL")
    assert err.value.event == game_has_not_started_event(game)
    game._engine_game = engine_game

    # Working
    # game.choose_resolution(active_player, "REVEAL")


def test_choose_player(lobby_with_game, mocker):
    lobby = lobby_with_game
    game = lobby.games[0]
    p1, p2, *_ = game.players
    engine_game_proceed = game.engine_game.proceed = mocker.Mock()

    # Player id not found
    with pytest.raises(TerminalEventError) as err:
        game.choose_player(p1, "Prison Mike")
    assert err.value.event == no_such_player_event("Prison Mike")

    # Working
    game.choose_player(p1, p2.id)
    engine_game_proceed.assert_any_call(game.engine_player(p1), ChoosePlayerUserInput(game.engine_player(p2)))


def test_choose_card(lobby_with_game, mocker):
    lobby = lobby_with_game
    game = lobby.games[0]
    p1, p2, *_ = game.players
    target_card = list(game.engine_player(p2).hand)[0]
    target_card_id = game.cards_ids[target_card]
    engine_proceed = game.engine_game.proceed = mocker.Mock()

    # Card id not found
    with pytest.raises(TerminalEventError) as err:
        game.choose_card(p1, "08.36.65.65.65")
    assert err.value.event == no_such_card_event("08.36.65.65.65")

    # Working
    game.choose_card(p1, target_card_id)
    engine_proceed.assert_any_call(game.engine_player(p1), ChooseCardUserInput(target_card))


def test_choose_destination(lobby_with_game, mocker):
    lobby = lobby_with_game
    game = lobby.games[0]
    p1, target_player, destination_player, *_ = game.players
    target_card = list(game.engine_player(target_player).hand)[0]
    target_card_id = game.cards_ids[target_card]
    destination_card = list(game.engine_player(destination_player).hand)[1]
    destination_card_id = game.cards_ids[destination_card]
    engine_proceed = game.engine_game.proceed = mocker.Mock()

    # Target card id not found
    with pytest.raises(TerminalEventError) as err:
        target_fake_card_id = "Come on guys. Early worm gets the worm"
        game.choose_destination(p1, target_fake_card_id, destination_card_id, Position.LEFT.name)
    assert err.value.event == no_such_card_event(target_fake_card_id)

    # Destination card id not found
    with pytest.raises(TerminalEventError) as err:
        destination_fake_id = "Another worm, like, are they friend ?"
        game.choose_destination(p1, target_card_id, destination_fake_id, Position.LEFT.name)
    assert err.value.event == no_such_card_event(destination_fake_id)

    # Wrong position
    with pytest.raises(TerminalEventError) as err:
        game.choose_destination(p1, target_card_id, destination_card_id, "IN ATTACK MODE")
    assert err.value.event == not_a_position_name_event("IN ATTACK MODE")

    # Working
    game.choose_destination(p1, target_card_id, destination_card_id, Position.LEFT.name)

    engine_proceed.assert_any_call(game.engine_player(p1), ChooseMovementUserInput(
        target_card, Position.LEFT, destination_card))


def test_game_chat(lobby_with_game):
    lobby = lobby_with_game
    game = lobby.games[-1]
    p1, *_ = lobby.logged_player

    msg = "a" * MAX_MESSAGE_LENGTH
    game.send_game_chat_message(p1, msg)

    for player in game.players:
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["message"] == msg
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["senderId"] == p1.id
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["gameName"] == game.name
        assert player.ws_wrapper.send.call_args_list[-1].args[0]["event"] == "GAME_CHAT_MESSAGE"
