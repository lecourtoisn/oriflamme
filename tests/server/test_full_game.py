from src.server.game import Game
from src.server.lobby import Lobby
from src.server.server import WebsocketWrapper


def test_full_game(lobby: Lobby):
    for name in "Dwight", "Michael", "Jim":
        lobby.login(WebsocketWrapper(None, []), name)
    dwight = lobby.players[0]
    michael = lobby.players[1]
    jim = lobby.players[2]

    game_name = "Flonkerton"
    lobby.create_game(dwight, game_name)
    lobby.join_game(michael, game_name)
    lobby.join_game(jim, game_name)

    Game.PLAYERS_CARD_NUMBER = 10
    Game.ROUND_NUMBER = 3
    lobby.start_game(dwight, game_name)

    game = lobby.games[-1]

    p1 = game.server_player(game.engine_game.players[0])
    p2 = game.server_player(game.engine_game.players[1])
    p3 = game.server_player(game.engine_game.players[2])

    game.play_card(p1, "soldat", "left")
    p1_soldat_id = game.cards_ids[game.engine_game.events[-2].card]
    game.play_card(p2, "heritier", "right")
    p2_heritier_id = game.cards_ids[game.engine_game.events[-2].card]
    game.play_card(p3, "complot", "right")
    # soldat1, heritier2, complot3
    game.choose_resolution(p1, "do_not_reveal")
    game.choose_resolution(p2, "reveal")
    game.choose_resolution(p3, "do_not_reveal")

    game.play_on(p2, "assassinat", p2_heritier_id)
    game.play_card(p3, "espion", "left")
    p3_espion_id = game.cards_ids[game.engine_game.events[-2].card]
    game.play_card(p1, "seigneur", "left")
    # heritier1, espion3, soldat1, (heritier2, assassin2), complot3
    game.choose_resolution(p1, "reveal")
    game.choose_resolution(p3, "reveal")
    game.choose_player(p3, p1.id)
    game.choose_resolution(p1, "reveal")
    game.choose_card(p1, p3_espion_id)
    game.choose_resolution(p2, "reveal")
    game.choose_card(p2, p1_soldat_id)
    game.choose_resolution(p3, "reveal")

    # heritier1, heritier2
    game.play_card(p3, "decret_royal", "left")
    p3_decret = game.cards_ids[game.engine_game.events[-2].card]
    game.play_card(p1, "embuscade", "right")
    p1_embuscade = game.cards_ids[game.engine_game.events[-2].card]
    game.play_card(p2, "archer", "left")
    # archer2, decret_royal3, heritier1, heritier2, embuscade1
    game.choose_resolution(p2, "reveal")
    game.choose_card(p2, p1_embuscade)
    game.choose_resolution(p3, "reveal")
    assert not game.over
    game.choose_destination(p3, p2_heritier_id, p3_decret, "left")
    assert game.over

    for p in dwight, michael, jim:
        with open(p.name + ".json", "w+") as f:
            import json
            events = [json.loads(d) for _, d in p.ws_wrapper.events]
            json.dump(events, f)
