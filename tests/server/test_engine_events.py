import json

from src.engine.card import CardName
from src.engine.enums import Position, RevealChoice
from src.server.engine_events import get_obfuscated_boards, player_played_card, resolution_player_turn, \
    placement_player_turn, prompt_card, prompt_player, prompt_movement, player_chose_resolution, \
    card_influence_update, player_influence_update, game_over, phase_starting, card_discarded, \
    revealed_card_activation, murder_countered, murdered_card, revealed_card, round_starting, game_status


def test_engine_events(mocker, lobby_with_game, p_cards):
    lobby = lobby_with_game
    game = lobby.games[-1]
    engine_game = game.engine_game

    p1, p2, p3, p4 = engine_game.players
    p1_cards, p2_cards, p3_cards, p4_cards = p_cards(p1), p_cards(p2), p_cards(p3), p_cards(p4)

    board = engine_game.board

    espion = p1_cards(CardName.ESPION)
    heritier = p2_cards(CardName.HERITIER)
    soldat = p3_cards(CardName.SOLDAT)
    seigneur = p3_cards(CardName.SEIGNEUR)
    archer = p1_cards(CardName.ARCHER)
    assassinat = p1_cards(CardName.ASSASSINAT)
    changeforme = p4_cards(CardName.CHANGEFORME)
    decret_royal = p4_cards(CardName.DECRET_ROYAL)
    embuscade = p4_cards(CardName.EMBUSCADE)
    complot = p2_cards(CardName.COMPLOT)

    heritier.influence = 4
    board.place(heritier, Position.RIGHT)

    espion.revealed = True
    board.place(espion, Position.RIGHT)

    soldat.influence = 3
    seigneur.influence = 2
    board.place(soldat, Position.RIGHT)
    board.place(seigneur, Position.ON, soldat)

    archer.revealed = True
    assassinat.revealed = True
    board.place(archer, Position.RIGHT)
    board.place(assassinat, Position.ON, archer)

    board.place(changeforme, Position.RIGHT)
    board.place(decret_royal, Position.ON, changeforme)
    board.place(embuscade, Position.ON, decret_royal)

    obf_boards = get_obfuscated_boards(game)

    for player, obf_board in obf_boards.items():
        engine_player = game.engine_player(player)
        for stack in obf_board:
            for card_json in stack:
                game_card = game.cards[card_json["cardId"]]
                assert card_json["revealed"] == game_card.revealed
                if card_json["revealed"] or engine_player == game_card.player:
                    assert "kind" in card_json
                else:
                    assert "kind" not in card_json

    for p in lobby.players:
        if p.ws_wrapper:
            mocker.patch.object(p.ws_wrapper, "send", new=lambda x, *y: json.dumps(x))

    for recipient_server_player in game.players:
        player_played_card(game, recipient_server_player, p1, espion, heritier, Position.RIGHT)
        resolution_player_turn(game, recipient_server_player, heritier)
        placement_player_turn(game, recipient_server_player, p1)
        prompt_card(game, recipient_server_player, complot)
        prompt_player(game, recipient_server_player, archer)
        prompt_movement(game, recipient_server_player, decret_royal)
        player_chose_resolution(game, recipient_server_player, RevealChoice.REVEAL, soldat)
        card_influence_update(game, recipient_server_player, seigneur, -4, 8)
        player_influence_update(game, recipient_server_player, p3, 8, 9)
        game_over(game, recipient_server_player)
        round_starting(game, recipient_server_player, 4)
        phase_starting(recipient_server_player, "PLACEMENT")
        phase_starting(recipient_server_player, "RESOLUTION")
        card_discarded(game, recipient_server_player, archer)
        revealed_card(game, recipient_server_player, soldat)
        revealed_card_activation(game, recipient_server_player, assassinat)
        murder_countered(game, recipient_server_player, assassinat, embuscade)
        murdered_card(game, recipient_server_player, soldat, seigneur)
        game_status(game, recipient_server_player)
