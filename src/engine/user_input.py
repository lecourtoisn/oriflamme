class UserInput:
    def __eq__(self, o: object) -> bool:
        for attr in dir(self):
            if not attr.startswith("_") and getattr(self, attr) != getattr(o, attr):
                return False

        return True


class PlacementUserInput(UserInput):
    def __init__(self, card_name, position, target_card=None):
        self.card_name = card_name
        self.position = position
        self.target_card = target_card


class RevealUserInput(UserInput):
    def __init__(self, reveal_choice):
        self.reveal_choice = reveal_choice


class ChoosePlayerUserInput(UserInput):
    def __init__(self, player):
        self.player = player


class ChooseCardUserInput(UserInput):
    def __init__(self, card):
        self.card = card


class ChooseMovementUserInput(UserInput):
    def __init__(self, target_card, position, destination_card):
        self.target_card = target_card
        self.position = position
        self.destination_card = destination_card
