from src.engine.enums import Position
from src.engine.event_manager import PromptPlayerEvent, StolenPlayerEvent, \
    MurderedCardEvent, PromptCardEvent, MurderCounteredEvent, PromptMovementEvent
from src.engine.exceptions import InvalidUserInputError, NeedUserInputError, MurderCounteredError
from src.engine.user_input import UserInput, ChoosePlayerUserInput, ChooseCardUserInput, ChooseMovementUserInput


class Ability:
    @classmethod
    def activate(cls, activating_card, board, user_input: UserInput):
        cls._activate(activating_card, board, user_input)

    @classmethod
    def _activate(cls, activating_card, board, user_input: UserInput):
        raise NotImplementedError

    @classmethod
    def validate(cls, activating_card, board, user_input):
        raise NotImplementedError

    @staticmethod
    def reveal(activating_card):
        activating_card.player.influence += activating_card.influence

    @staticmethod
    def counter_murder(murderer, target, board):
        pass


class InputAbility(Ability):
    @classmethod
    def activate(cls, activating_card, board, user_input: UserInput):
        cls._activate(activating_card, board, user_input)

    @classmethod
    def _activate(cls, activating_card, board, user_input: UserInput):
        raise NotImplementedError

    @classmethod
    def validate(cls, activating_card, board, user_input):
        if user_input is None:
            raise NeedUserInputError(cls._need_input_event(activating_card, board))
        if not isinstance(user_input, cls._input_type_needed()):
            raise InvalidUserInputError
        cls._validate(activating_card, board, user_input)

    @staticmethod
    def _need_input_event(activating_card, board):
        raise NotImplementedError

    @staticmethod
    def _validate(activating_card, board, user_input):
        raise NotImplementedError

    @staticmethod
    def _input_type_needed():
        raise NotImplementedError


class NoInputAbility(Ability):
    @classmethod
    def _activate(cls, activating_card, board, user_input: UserInput):
        cls._activate_no_input(activating_card, board)

    @classmethod
    def validate(cls, activating_card, board, user_input):
        # If the ability doesn't need an input, the game shouldn't be paused and the user shouldn't be able
        # to send an input. But if for whatever reason we end up here, it's fine. I don't need an input,
        # I will ignore it anyway
        if user_input is not None:
            raise Exception("I DON'T WANT ANY INPUT")
        pass

    @staticmethod
    def _activate_no_input(activating_card, board):
        raise NotImplementedError


class Murderer(InputAbility):
    @classmethod
    def _activate(cls, activating_card, board, user_input: ChooseCardUserInput):
        if user_input.card.player == activating_card.player:
            user_input.card.reveal()
        try:
            user_input.card.ability.counter_murder(activating_card, user_input.card, board)
        except MurderCounteredError:
            activating_card.player.events.append(MurderCounteredEvent(activating_card, user_input.card))
        else:
            board.discard(user_input.card)
            activating_card.player.events.append(MurderedCardEvent(activating_card, user_input.card))
        activating_card.player.influence += 1

    @staticmethod
    def _validate(activating_card, board, user_input):
        raise NotImplementedError

    @staticmethod
    def _need_input_event(activating_card, board):
        return PromptCardEvent(activating_card)

    @staticmethod
    def _input_type_needed():
        return ChooseCardUserInput


class Assassinat(Murderer):
    @classmethod
    def _activate(cls, activating_card, board, user_input: ChooseCardUserInput):
        Murderer._activate(activating_card, board, user_input)
        board.discard(activating_card)

    @staticmethod
    def _validate(activating_card, board, user_input):
        valid_cards = board.uncovered_cards
        if user_input.card not in valid_cards:
            raise InvalidUserInputError


class Soldat(Murderer):
    @staticmethod
    def _validate(activating_card, board, user_input):
        valid_cards = {*board.adjacent_cards(activating_card), activating_card}
        if user_input.card not in valid_cards:
            raise InvalidUserInputError


class Archer(Murderer):
    @staticmethod
    def _validate(activating_card, board, user_input):
        valid_cards = {*board.extremities(), activating_card}
        if user_input.card not in valid_cards:
            raise InvalidUserInputError


class Espion(InputAbility):
    @classmethod
    def _activate(cls, activating_card, board, user_input: ChoosePlayerUserInput):
        activating_card.player.events.append(StolenPlayerEvent(activating_card, user_input.player, 1))
        if activating_card.player != user_input.player:
            user_input.player.influence -= 1
            activating_card.player.influence += 1

    @staticmethod
    def _validate(activating_card, board, user_input: ChoosePlayerUserInput):
        valid_players = [card.player for card in board.adjacent_cards(activating_card) if
                         card.player.influence > 0]
        valid_players.append(activating_card.player)
        if user_input.player not in valid_players:
            raise InvalidUserInputError

    @staticmethod
    def _need_input_event(activating_card, board):
        return PromptPlayerEvent(activating_card)

    @staticmethod
    def _input_type_needed():
        return ChoosePlayerUserInput


class Heritier(NoInputAbility):
    @classmethod
    def _activate_no_input(cls, activating_card, board):
        other_revealed_heritier = [
            card for card in board if
            card.revealed and card.player != activating_card.player and card.name == activating_card.name
        ]

        if not other_revealed_heritier:
            activating_card.player.influence += 2


class DecretRoyal(InputAbility):
    @classmethod
    def _activate(cls, activating_card, board, user_input: ChooseMovementUserInput):
        board.move(user_input.target_card, user_input.position, user_input.destination_card)
        board.discard(activating_card)

    @staticmethod
    def _validate(activating_card, board, user_input: ChooseMovementUserInput):
        if user_input.target_card not in board.uncovered_cards \
                or user_input.destination_card not in board.uncovered_cards \
                or user_input.position not in [Position.LEFT, Position.RIGHT]:
            raise InvalidUserInputError

    @staticmethod
    def _need_input_event(activating_card, board):
        return PromptMovementEvent(activating_card)

    @staticmethod
    def _input_type_needed():
        return ChooseMovementUserInput


class Seigneur(NoInputAbility):
    @staticmethod
    def _activate_no_input(activating_card, board):
        adjacent_cards = board.adjacent_cards(activating_card)
        influence_won = 1 + len([card for card in adjacent_cards if card.player == activating_card.player])
        activating_card.player.influence += influence_won


class Embuscade(NoInputAbility):
    @staticmethod
    def _activate_no_input(activating_card, board):
        board.discard(activating_card)

    @staticmethod
    def reveal(activating_card):
        activating_card.player.influence += 1

    @staticmethod
    def counter_murder(murderer, target, board):
        board.discard(target)
        if murderer.player != target.player:
            board.discard(murderer)
            target.player.influence += 4
        raise MurderCounteredError


class Complot(NoInputAbility):
    @staticmethod
    def _activate_no_input(activating_card, board):
        board.discard(activating_card)

    @staticmethod
    def reveal(activating_card):
        activating_card.player.influence += 2 * activating_card.influence


class Changeforme(InputAbility):
    class ChangeFormCopiedAbility:
        def __init__(self, copied_card, changeforme):
            self.copied_ability = copied_card.ability
            self.changeforme = changeforme

        def activate(self, *args, **kwargs):
            self.copied_ability.activate(*args, **kwargs)
            self.changeforme.ability = Changeforme

        def validate(self, *args, **kwargs):
            self.copied_ability.validate(*args, **kwargs)

    @classmethod
    def _activate(cls, activating_card, board, user_input: ChooseCardUserInput):
        if user_input.card.ability != activating_card.ability:
            activating_card.ability = Changeforme.ChangeFormCopiedAbility(user_input.card, activating_card)
            activating_card.validate(board, None)
            activating_card.activate(board, None)

    @staticmethod
    def _validate(activating_card, board, user_input):
        valid_cards = {
            *{card for card in board.adjacent_cards(activating_card) if card.revealed},
            activating_card}
        if user_input.card not in valid_cards:
            raise InvalidUserInputError

    @staticmethod
    def _need_input_event(activating_card, board):
        return PromptCardEvent(activating_card)

    @staticmethod
    def _input_type_needed():
        return ChooseCardUserInput
