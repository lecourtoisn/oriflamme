class GameEvent:
    pass


class PlacementPlayerTurnEvent(GameEvent):
    def __init__(self, player):
        self.player = player
        self.prompt_type = "PLACEMENT"


class ResolutionPlayerTurnEvent(GameEvent):
    def __init__(self, card):
        self.card = card
        self.prompt_type = "REVEAL"


class PromptCardEvent(GameEvent):
    def __init__(self, card):
        self.card = card
        self.prompt_type = "CARD"


class PromptPlayerEvent(GameEvent):
    def __init__(self, card):
        self.card = card
        self.prompt_type = "PLAYER"


class PromptMovementEvent(GameEvent):
    def __init__(self, card):
        self.card = card
        self.prompt_type = "MOVEMENT"


class PlayerPlayedCardEvent(GameEvent):
    def __init__(self, player, card, relative_card, position):
        self.player = player
        self.card = card
        self.relative_card = relative_card
        self.position = position


class CardMovedEvent(GameEvent):
    def __init__(self, moved_card, target_card, position):
        self.moved_card = moved_card
        self.target_card = target_card
        self.position = position


class PlayerChoseRevealEvent(GameEvent):
    def __init__(self, card, resolution):
        self.card = card
        self.resolution = resolution


class CardInfluenceUpdateEvent(GameEvent):
    def __init__(self, card, difference, value):
        self.card = card
        self.difference = difference
        self.value = value


class PlayerInfluenceUpdateEvent(GameEvent):
    def __init__(self, player, difference, value):
        self.player = player
        self.difference = difference
        self.value = value


class GameOverEvent(GameEvent):
    pass


class RoundStartingEvent(GameEvent):
    def __init__(self, round_number):
        self.round_number = round_number


class PlacementPhaseStartingEvent(GameEvent):
    pass


class ResolutionPhaseStartingEvent(GameEvent):
    pass


class CardDiscardedEvent(GameEvent):
    def __init__(self, card):
        self.card = card


class RevealedCardEvent(GameEvent):
    def __init__(self, card):
        self.card = card


class RevealedCardActivationEvent(GameEvent):
    def __init__(self, card):
        self.card = card


class MurderCounteredEvent(GameEvent):
    def __init__(self, murderer, target_card):
        self.murderer = murderer
        self.target_card = target_card


class MurderedCardEvent(GameEvent):
    def __init__(self, murderer, target_card):
        self.murderer = murderer
        self.target_card = target_card


class StolenPlayerEvent(GameEvent):
    def __init__(self, choosing_card, target_player, loss):
        self.choosing_card = choosing_card
        self.target_player = target_player
        self.loss = loss


class GameStatusEvent(GameEvent):
    pass
