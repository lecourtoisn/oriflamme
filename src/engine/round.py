from src.engine.exceptions import NeedUserInputError, PhaseOverError, RoundOverError
from src.engine.phase import PlacementPhase, ResolutionPhase
from src.engine.player import Player
from src.engine.user_input import UserInput


class Round:
    def __init__(self, events, players, board):
        self.players = players.copy()
        self.placement_phase = PlacementPhase(events, players, board)
        self.resolution_phase = ResolutionPhase(events, board)
        self.phase = self.placement_phase

    def proceed(self, player: Player = None, user_input: UserInput = None):
        try:
            if not self.phase.is_started:
                self.phase.start()
            else:
                self.phase.proceed(player, user_input)
        except NeedUserInputError:
            # I think that later phases won't raise NeedUserInputError because they can handle it themselves.
            # Right now it's only raised to simplify tests because I can't test that they handle it correctly
            # meaning adding the right event.
            pass
        except PhaseOverError:
            # Adding an event when it's implemented
            if self.phase == self.placement_phase:
                self.phase = self.resolution_phase
                self.proceed()
            else:
                self.phase = None
                raise RoundOverError

    @property
    def phase_name(self):
        return "PLACEMENT" if isinstance(self.phase, PlacementPhase) else "RESOLUTION"
