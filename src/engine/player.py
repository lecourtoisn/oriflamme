from random import sample
from typing import Set

from src.engine.board import Board
from src.engine.card import Card
from src.engine.enums import CardName
from src.engine.event_manager import PlayerInfluenceUpdateEvent, PlayerPlayedCardEvent
from src.engine.exceptions import NumberOfCardsAskedTooHigh, CardNotInPlayerHandError


class Player:
    def __init__(self, events, color, number_cards=7):
        self._influence = 1
        self.color = color
        self.events = events

        try:
            # noinspection PyTypeChecker
            random_sample_of_cards_names = sample(list(CardName), number_cards)
        except ValueError:
            raise NumberOfCardsAskedTooHigh

        self._hand = {card_name: Card(self, card_name) for card_name in random_sample_of_cards_names}

    @property
    def influence(self):
        return self._influence

    @influence.setter
    def influence(self, value):
        difference = value - self._influence
        self._influence = value
        if difference != 0:
            self.events.append(PlayerInfluenceUpdateEvent(self, difference, value))

    def play_card_name(self, board: Board, card_name: CardName, position, target_card: Card = None):
        try:
            card_to_be_played = self._hand[card_name]
        except KeyError:
            raise CardNotInPlayerHandError

        board.place(card_to_be_played, position, target_card)
        self.events.append(PlayerPlayedCardEvent(self, card_to_be_played, target_card, position))
        del self._hand[card_name]

    @property
    def hand(self) -> Set[Card]:
        return set(self._hand.values())

    def __str__(self) -> str:
        return f"Player {self.color.name}"
