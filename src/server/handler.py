import inspect
from collections import namedtuple, defaultdict


class ActionHandler:
    ACTIONS = defaultdict(dict)

    @staticmethod
    def action(logged=True):
        def deco(function):
            def func(*args, **kwargs):
                return function(*args, **kwargs)

            needed_args = [Param(param.name, param.default) for param in
                           inspect.signature(function).parameters.values()]
            cls_name = function.__qualname__.split('.')[0]
            ActionHandler.ACTIONS[cls_name][function.__name__] = {
                "function": func,
                "logged": logged,
                "params": [
                    param
                    for param in needed_args
                    if param.name not in ("self", "ws_wrapper", "player")]}

            return func

        return deco

    @staticmethod
    def get_handler(server, params):
        pass

    @classmethod
    def actions(cls):
        return ActionHandler.ACTIONS[cls.__name__]


Param = namedtuple("Param", ["name", "default"])
