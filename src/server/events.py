def parameter_missing_event(parameter, action=None):
    event = {
        "event": "PARAMETER_MISSING",
        "parameter": parameter
    }
    if action is not None:
        event["action"] = action
    return event


def invalid_json_syntax_event(data):
    return {
        "event": "INVALID_JSON_SYNTAX",
        "data": data
    }


def no_such_action_event(action):
    return {
        "event": "NO_SUCH_ACTION",
        "action": action
    }


def no_such_logged_player_event(session_token):
    return {
        "event": "NO_SUCH_LOGGED_PLAYER",
        "sessionToken": session_token
    }


def no_such_player_event(player_id):
    return {
        "event": "NO_SUCH_PLAYER",
        "playerId": player_id
    }


def game_already_exists_event(game_name):
    return {
        "event": "GAME_ALREADY_EXISTS",
        "gameName": game_name
    }


def no_such_game_event(game_name):
    return {
        "event": "NO_SUCH_GAME",
        "gameName": game_name
    }


def player_not_in_game_event(game, player):
    return {
        "event": "PLAYER_NOT_IN_GAME",
        "playerId": player.id,
        "gameName": game.name
    }


def player_already_in_a_game_event(player, game):
    return {
        "event": "PLAYER_ALREADY_IN_GAME",
        "playerId": player.id,
        "gameName": game.name
    }


def disconnected_event(player):
    return {
        "event": "DISCONNECTED",
        "playerId": player.id
    }


def game_created_event(game):
    return {
        "event": "GAME_CREATED",
        "game": game.as_dict(),
    }


def player_joined_game_event(player, game):
    return {
        "event": "PLAYER_JOINED_GAME",
        "playerId": player.id,
        "gameName": game.name
    }


def player_left_game_event(player, game):
    return {
        "event": "PLAYER_LEFT_GAME",
        "playerId": player.id,
        "gameName": game.name,
        "gameLeaderId": game.leader.id if game.leader is not None else None
    }


def player_joined_lobby_event(player):
    return {
        "event": "PLAYER_JOINED_LOBBY",
        "player": player.as_dict()
    }


def player_session_token_event(player, lobby):
    return {
        "event": "PLAYER_SESSION_TOKEN",
        "sessionToken": player.session_token,
        "lobby": lobby.as_dict()
    }


def game_deleted_event(game_name):
    return {
        "event": "GAME_DELETED",
        "gameName": game_name
    }


def max_player_limit_event(max_nb_player):
    return {
        "event": "MAX_PLAYER_LIMIT",
        "maxNbPlayer": max_nb_player
    }


def not_enough_players_event(min_nb_player):
    return {
        "event": "NOT_ENOUGH_PLAYERS",
        "minNbPlayer": min_nb_player
    }


def player_is_not_game_leader_event(player):
    return {
        "event": "PLAYER_IS_NOT_GAME_LEADER",
        "playerId": player.id
    }


def game_already_started_event(game):
    return {
        "event": "GAME_ALREADY_STARTED",
        "gameName": game.name
    }


def game_has_not_started_event(game):
    return {
        "event": "GAME_AS_NOT_STARTED",
        "gameName": game.name
    }


def not_a_card_name_event(card_name):
    return {
        "event": "NOT_A_CARD_NAME",
        "cardName": card_name
    }


def no_such_card_event(card_id):
    return {
        "event": "NO_SUCH_CARD",
        "cardId": card_id
    }


def not_a_position_name_event(position_name):
    return {
        "event": "NOT_A_POSITION_NAME",
        "positionName": position_name
    }


def not_a_resolution_choice_name_event(resolution_choice):
    return {
        "event": "INVALID_RESOLUTION_CHOICE",
        "resolutionChoice": resolution_choice
    }


def game_engine_error_event(game_engine_error):
    return {
        "event": "GAME_ENGINE_ERROR",
        "gameEngineError": game_engine_error
    }


def chat_message_too_long_event():
    return {
        "event": "MESSAGE_TOO_LONG"
    }


def lobby_chat_message_event(message, sender):
    return {
        "event": "LOBBY_CHAT_MESSAGE",
        "senderId": sender.id,
        "message": message
    }


def game_chat_message_event(message, sender, game):
    return {
        "event": "GAME_CHAT_MESSAGE",
        "senderId": sender.id,
        "message": message,
        "gameName": game.name
    }


class TerminalEventError(Exception):
    def __init__(self, event):
        self.event = event


class NoSuchGame(TerminalEventError):
    def __init__(self, game_name):
        self.game_name = game_name
        self.event = no_such_game_event(self.game_name)
