import uuid


class Player:
    def __init__(self, name):
        self.id = str(uuid.uuid4())
        self.name = name
        self.ws_wrapper = None
        self.session_token = None

    @property
    def logged(self):
        return self.ws_wrapper is not None

    def login(self, ws_wrapper, token=None):
        self.ws_wrapper = ws_wrapper
        self.session_token = token or str(uuid.uuid4())

    def logoff(self):
        self.ws_wrapper = None

    def as_dict(self, engine_player=None):
        player_dict = {
            "id": self.id,
            "name": self.name,
            "logged": self.logged
        }
        if engine_player is not None:
            player_dict["color"] = engine_player.color.name
            player_dict["influence"] = engine_player.influence
        return player_dict

    def __str__(self):
        return self.name

    def send(self, event):
        if self.logged:
            self.ws_wrapper.send(event, self)
