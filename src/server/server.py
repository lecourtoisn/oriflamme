import inspect
import json
import logging

import inflection
import websockets
from websockets import ConnectionClosed

from src.engine.exceptions import OriflammeError
from src.server.events import parameter_missing_event, invalid_json_syntax_event, no_such_action_event, \
    TerminalEventError, game_engine_error_event
from src.server.game import Game
from src.server.handler import Param
from src.server.lobby import Lobby

logger = logging.getLogger('websockets')


class WebsocketWrapper:
    def __init__(self, websocket, events):
        self.websocket = websocket
        self.events = events

    def send(self, json_dict, player=None):
        if player is not None:
            json_dict["recipientId"] = player.id
        self.events.append((self.websocket, json.dumps(json_dict)))


class Server:
    def __init__(self, port, loop):
        self.port = port
        self.event_loop = loop
        self.lobby = Lobby(self)
        self.events = []

    def process_message(self, ws_wrapper, str_data):
        player = None
        try:
            try:
                json_data = {inflection.underscore(key): value for key, value in json.loads(str_data).items()}
                action_name = json_data["action"]
            except json.JSONDecodeError:
                raise TerminalEventError(invalid_json_syntax_event(str_data))
            except KeyError as err:
                raise TerminalEventError(parameter_missing_event(err.args[0]))
            else:
                action_name = action_name.lower().strip()
                try:
                    handler = [h for h in (Lobby, Game) if action_name in h.actions()][0]
                except IndexError:
                    raise TerminalEventError(no_such_action_event(action_name))

                action = handler.actions()[action_name]
                function = action["function"]
                params = action["params"]
                need_logged = action["logged"]
                extra_params = []
                if need_logged:
                    extra_params.append(Param("session_token", inspect.Parameter.empty))
                if handler is Game:
                    extra_params.append(Param("game_name", inspect.Parameter.empty))
                try:
                    kwargs = {
                        param.name:
                            json_data[param.name] if param.default == inspect.Parameter.empty else
                            json_data.get(param.name, param.default)
                        for param in params + extra_params}
                except KeyError as err:
                    raise TerminalEventError(parameter_missing_event(err.args[0], action_name))

                handler_instance = handler.get_handler(self, kwargs)
                minimal_params = {function_param: kwargs[function_param] for function_param, value in params}
                try:
                    if need_logged:
                        player = self.lobby.find_player(kwargs["session_token"])
                        function(handler_instance, player, **minimal_params)
                    else:
                        function(handler_instance, ws_wrapper, **minimal_params)
                except OriflammeError as err:
                    raise TerminalEventError(game_engine_error_event(err.__class__.__name__))
        except TerminalEventError as err:
            ws_wrapper.send(err.event, player)

    async def send_events(self):
        for ws, event in self.events:
            try:
                await ws.send(event)
            except ConnectionClosed:
                logger.info("Connection closed with a socket while processing a message")
        self.events.clear()

    async def main_loop(self, websocket, _):
        ws_wrapper = WebsocketWrapper(websocket, self.events)
        try:
            async for message in websocket:
                self.process_message(ws_wrapper, message)
                await self.send_events()
        except websockets.ConnectionClosedError:
            pass
        finally:
            # Arriving here mean the line 'async for message in websocket' raised. So self.events must be empty
            # so no risk of sending already sent events
            # self.lobby.logoff populates self.events with logoff events
            # then they're sent.
            for player in self.lobby.players:
                if player.ws_wrapper == ws_wrapper:
                    self.lobby.logoff(player)
            await self.send_events()

    def start(self):
        # noinspection PyTypeChecker
        start_server = websockets.serve(self.main_loop, "0.0.0.0", self.port)
        self.event_loop.run_until_complete(start_server)
