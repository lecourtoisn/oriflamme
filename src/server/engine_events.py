from src.engine.event_manager import PlacementPlayerTurnEvent, ResolutionPlayerTurnEvent, PromptCardEvent, \
    PromptPlayerEvent, PromptMovementEvent, PlayerPlayedCardEvent, PlayerChoseRevealEvent, CardInfluenceUpdateEvent, \
    PlayerInfluenceUpdateEvent, GameOverEvent, RoundStartingEvent, PlacementPhaseStartingEvent, \
    ResolutionPhaseStartingEvent, CardDiscardedEvent, RevealedCardEvent, RevealedCardActivationEvent, \
    MurderCounteredEvent, MurderedCardEvent, GameStatusEvent, CardMovedEvent


def get_obfuscated_boards(game):
    obfuscated_boards = {}
    for player in game.players:
        obfuscated_board = []
        for stack in game.engine_game.board.stacks:
            stack_obj = []
            for card in stack:
                card_server_player = game.server_player(card.player)
                card_obj = card_as_dict(game, card, player)
                if card.revealed or card_server_player == player:
                    card_obj["kind"] = card.name.name
                stack_obj.append(card_obj)
            obfuscated_board.append(stack_obj)
        obfuscated_boards[player] = obfuscated_board
    return obfuscated_boards


def card_as_dict(game, card, recipient):
    server_player = game.server_player(card.player)
    card_dict = {
        "cardId": game.cards_ids[card],
        "playerId": server_player.id,
        "revealed": card.revealed
    }
    if card.revealed or recipient == server_player:
        card_dict["kind"] = card.name.name
    if not card.revealed:
        card_dict["influence"] = card.influence

    return card_dict


def placement_player_turn(game, recipient_server_player, player):
    server_player = game.server_player(player)
    recipient_server_player.send({
        "event": "PLACEMENT_PLAYER_TURN",
        "playerId": server_player.id,
    })


def resolution_player_turn(game, recipient_server_player, card):
    server_player = game.server_player(card.player)
    event = {
        "event": "RESOLUTION_PLAYER_TURN",
        "playerId": server_player.id,
        "cardId": game.cards_ids[card]
    }
    recipient_server_player.send(event)


def prompt_card(game, recipient_server_player, card):
    server_player = game.server_player(card.player)

    event = {
        "event": "PROMPT_CARD",
        "playerId": server_player.id,
        "cardId": game.cards_ids[card]
    }
    recipient_server_player.send(event)


def prompt_player(game, recipient_server_player, card):
    server_player = game.server_player(card.player)
    event = {
        "event": "PROMPT_PLAYER",
        "playerId": server_player.id,
        "cardId": game.cards_ids[card]
    }
    recipient_server_player.send(event)


def prompt_movement(game, recipient_server_player, card):
    server_player = game.server_player(card.player)
    event = {
        "event": "PROMPT_MOVEMENT",
        "playerId": server_player.id,
        "cardId": game.cards_ids[card]
    }
    recipient_server_player.send(event)


def player_chose_resolution(game, recipient_server_player, resolution, card):
    server_player = game.server_player(card.player)
    recipient_server_player.send({
        "event": "PLAYER_CHOSE_RESOLUTION",
        "playerId": server_player.id,
        "resolution": resolution.name,
        "cardId": game.cards_ids[card],
    })


def card_influence_update(game, recipient_server_player, card, difference, value):
    card_id = game.cards_ids[card]
    recipient_server_player.send({
        "event": "CARD_INFLUENCE_UPDATE",
        "cardId": card_id,
        "difference": difference,
        "influence": value
    })


def player_influence_update(game, recipient_server_player, player, difference, value):
    server_player = game.server_player(player)
    recipient_server_player.send({
        "event": "PLAYER_INFLUENCE_UPDATE",
        "playerId": server_player.id,
        "difference": difference,
        "influence": value
    })


def game_over(game, recipient_server_player):
    scores = {game.server_player(player).id: player.influence for player in game.engine_game.players}

    recipient_server_player.send({
        "event": "GAME_OVER",
        "scores": scores,
        "gameName": game.name
    })


def round_starting(game, recipient_server_player, round_number):
    actual_round_number = round_number + 1
    recipient_server_player.send({
        "event": "ROUND_STARTING",
        "round": {
            "roundNumber": actual_round_number,
            "playersOrder": [game.server_player(player).id for player in game.engine_game.active_round.players],
        },
    })


def phase_starting(recipient_server_player, phase):
    recipient_server_player.send({
        "event": "PHASE_STARTING",
        "phase": phase
    })


def card_discarded(game, recipient_server_player, card):
    card_dict = card_as_dict(game, card, recipient_server_player)
    recipient_server_player.send({
        "event": "CARD_DISCARDED",
        "card": card_dict
    })


def revealed_card(game, recipient_server_player, card):
    recipient_server_player.send({
        "event": "REVEALED_CARD",
        "card": card_as_dict(game, card, recipient_server_player)
    })


def revealed_card_activation(game, recipient_server_player, card):
    recipient_server_player.send({
        "event": "REVEALED_CARD_ACTIVATION",
        "cardId": game.cards_ids[card]
    })


def murder_countered(game, recipient_server_player, murderer, target_card):
    recipient_server_player.send({
        "event": "MURDER_COUNTERED",
        "murdererCardId": game.cards_ids[murderer],
        "targetedCardId": game.cards_ids[target_card]
    })


def murdered_card(game, recipient_server_player, murderer, target_card):
    recipient_server_player.send({
        "event": "MURDERED_CARD",
        "murdererCardId": game.cards_ids[murderer],
        "targetedCardId": game.cards_ids[target_card]
    })


def player_played_card(game, recipient_server_player, player, card, relative_card, position):
    server_player = game.server_player(player)
    relative_card_id = None if relative_card is None else game.cards_ids[relative_card]
    recipient_server_player.send({
        "event": "PLAYER_PLAYED_CARD",
        "playerId": server_player.id,
        "card": card_as_dict(game, card, recipient_server_player),
        "positionName": position.name,
        "relativeCardId": relative_card_id
    })


def card_moved(game, recipient_server_player, moved_card, targed_card, position):
    recipient_server_player.send({
        "event": "CARD_MOVED",
        "movedCardId": game.cards_ids[moved_card],
        "targetCardId": game.cards_ids[targed_card],
        "positionName": position.name
    })


def game_status(game, recipient_server_player):
    obfuscated_boards = get_obfuscated_boards(game)
    engine_player = game.engine_player(recipient_server_player)

    phase_name = game.engine_game.active_round.phase_name
    round_number = game.engine_game.round_counter + 1
    hand = [card_as_dict(game, card, recipient_server_player) for card in engine_player.hand]
    engine_player_turn = game.engine_game.active_round.phase.player_turn
    server_player_turn = game.server_player(engine_player_turn)
    active_card = game.engine_game.active_round.resolution_phase.active_card
    active_card_id = None if phase_name == "PLACEMENT" else game.cards_ids[active_card]

    event = {
        "event": "GAME_STATUS",
        "players": [game.server_player(player).as_dict(player) for player in game.engine_game.players],
        "hand": hand,
        "board": obfuscated_boards[recipient_server_player],
        "phase": {
            "phaseName": phase_name,
            "activeCardId": active_card_id,
            "prompt": game.engine_game.active_round.phase.current_prompt_input_type
        },
        "round": {
            "roundNumber": round_number,
            "playersOrder": [game.server_player(player).id for player in game.engine_game.active_round.players],
        },
        "playerTurnId": server_player_turn.id
    }
    recipient_server_player.send(event)


class EngineEventsHandler:
    def __init__(self, game):
        self.game = game
        self.event_iter = 0

    def send_events(self):
        events = self.game.engine_game.events
        for event in events[self.event_iter: len(events)]:
            for recipient_server_player in self.game.players:
                self._handle_event(event, recipient_server_player)

        self.event_iter = len(events)

    def _handle_event(self, event, recipient_server_player):
        event_kind = type(event)
        if event_kind == PlacementPlayerTurnEvent:
            placement_player_turn(self.game, recipient_server_player, event.player)
        elif event_kind == ResolutionPlayerTurnEvent:
            resolution_player_turn(self.game, recipient_server_player, event.card)
        elif event_kind == PromptCardEvent:
            prompt_card(self.game, recipient_server_player, event.card)
        elif event_kind == PromptPlayerEvent:
            prompt_player(self.game, recipient_server_player, event.card)
        elif event_kind == PromptMovementEvent:
            prompt_movement(self.game, recipient_server_player, event.card)
        elif event_kind == PlayerPlayedCardEvent:
            player_played_card(self.game, recipient_server_player, event.player, event.card, event.relative_card,
                               event.position)
        elif event_kind == CardMovedEvent:
            card_moved(self.game, recipient_server_player, event.moved_card, event.target_card, event.position)
        elif event_kind == PlayerChoseRevealEvent:
            player_chose_resolution(self.game, recipient_server_player, event.resolution, event.card)
        elif event_kind == CardInfluenceUpdateEvent:
            card_influence_update(self.game, recipient_server_player, event.card, event.difference, event.value)
        elif event_kind == PlayerInfluenceUpdateEvent:
            player_influence_update(self.game, recipient_server_player, event.player, event.difference, event.value)
        elif event_kind == GameOverEvent:
            game_over(self.game, recipient_server_player)
        elif event_kind == RoundStartingEvent:
            round_starting(self.game, recipient_server_player, event.round_number)
        elif event_kind == PlacementPhaseStartingEvent:
            phase_starting(recipient_server_player, "PLACEMENT")
        elif event_kind == ResolutionPhaseStartingEvent:
            phase_starting(recipient_server_player, "RESOLUTION")
        elif event_kind == CardDiscardedEvent:
            card_discarded(self.game, recipient_server_player, event.card)
        elif event_kind == RevealedCardEvent:
            revealed_card(self.game, recipient_server_player, event.card)
        elif event_kind == RevealedCardActivationEvent:
            revealed_card_activation(self.game, recipient_server_player, event.card)
        elif event_kind == MurderCounteredEvent:
            murder_countered(self.game, recipient_server_player, event.murderer, event.target_card)
        elif event_kind == MurderedCardEvent:
            murdered_card(self.game, recipient_server_player, event.murderer, event.target_card)
        # elif event_kind == StolenPlayerEvent:
        #     stolen_player(self.game, recipient_server_player, event.choosing_card, event.target_player, event.loss)
        elif event_kind == GameStatusEvent:
            game_status(self.game, recipient_server_player)
