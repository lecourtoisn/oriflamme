from uuid import uuid4

from src.engine.board import Position
from src.engine.card import CardName
from src.engine.enums import RevealChoice
from src.engine.user_input import ChooseCardUserInput, PlacementUserInput, RevealUserInput, ChoosePlayerUserInput, \
    ChooseMovementUserInput
from src.server.chat import send_game_message
from src.server.engine_events import EngineEventsHandler
from src.server.events import TerminalEventError, player_not_in_game_event, game_has_not_started_event, \
    not_a_card_name_event, not_a_position_name_event, not_a_resolution_choice_name_event, \
    no_such_player_event, no_such_card_event
from src.server.handler import ActionHandler


# noinspection PyNoneFunctionAssignment
class Game(ActionHandler):
    PLAYERS_CARD_NUMBER = 7
    MINIMUM_PLAYER_NUMBER = 3
    MAXIMUM_PLAYER_NUMBER = 5
    ROUND_NUMBER = 6

    def __init__(self, name, leader):
        self.name = name
        self._engine_game = None
        self._engine_players = {leader: None}
        self.cards = {}
        self.cards_ids = {}
        self.event_handler = EngineEventsHandler(self)

    @property
    def players(self):
        return tuple(self._engine_players.keys())

    def define_cards_ids(self):
        for engine_player in self._engine_players.values():
            for card in engine_player.hand:
                uuid = str(uuid4())
                self.cards[uuid] = card
                self.cards_ids[card] = uuid

    def engine_player(self, player):
        if not self.started:
            raise TerminalEventError(game_has_not_started_event(self))
        try:
            return self._engine_players[player]
        except KeyError:
            raise TerminalEventError(player_not_in_game_event(self, player))

    def server_player(self, engine_player):
        return [server_p for server_p, engine_p in self._engine_players.items() if engine_p == engine_player][0]

    def add_player(self, player, engine_player=None):
        self._engine_players[player] = engine_player

    def remove_player(self, player):
        try:
            del self._engine_players[player]
        except KeyError:
            raise TerminalEventError(player_not_in_game_event(self, player))

    @property
    def engine_game(self):
        if not self.started:
            raise TerminalEventError(game_has_not_started_event(self))
        return self._engine_game

    @property
    def leader(self):
        return self.players[0] if self.players else None

    @property
    def started(self):
        return self._engine_game is not None

    @property
    def over(self):
        return self.started and self.engine_game.is_over

    @ActionHandler.action()
    def play_card(self, player, card_name, position_name):
        """
        :param player:
        :param card_name: Ex: SOLDAT
        :param position_name: Either RIGHT, LEFT or the name of another card
        :return:
        """
        card_kind = self.identify_card_kind(card_name)
        position = self.identify_position(position_name)

        engine_player = self.engine_player(player)
        self.engine_game.proceed(engine_player, PlacementUserInput(card_kind, position))

        self.event_handler.send_events()

    @ActionHandler.action()
    def play_on(self, player, card_name, target_card_id):
        card_kind = self.identify_card_kind(card_name)
        engine_player = self.engine_player(player)
        target_card = self.identify_card(target_card_id)
        self.engine_game.proceed(engine_player, PlacementUserInput(card_kind, Position.ON, target_card))

        self.event_handler.send_events()

    @ActionHandler.action()
    def choose_resolution(self, player, resolution_choice):
        choice = self.identify_resolution_choice(resolution_choice)
        self.engine_game.proceed(self.engine_player(player), RevealUserInput(choice))

        self.event_handler.send_events()

    @ActionHandler.action()
    def choose_player(self, player, target_player_id):
        target_server_player = self.identify_player(target_player_id)
        engine_target_player = self.engine_player(target_server_player)
        engine_playing_player = self.engine_player(player)
        self.engine_game.proceed(engine_playing_player, ChoosePlayerUserInput(engine_target_player))

        self.event_handler.send_events()

    @ActionHandler.action()
    def choose_card(self, player, card_id):
        card = self.identify_card(card_id)
        engine_playing_player = self.engine_player(player)
        self.engine_game.proceed(engine_playing_player, ChooseCardUserInput(card))

        self.event_handler.send_events()

    @ActionHandler.action()
    def choose_destination(self, player, target_card_id, destination_card_id, position_name):
        target_card = self.identify_card(target_card_id)
        destination_card = self.identify_card(destination_card_id)
        position = self.identify_position(position_name)
        movement_input = ChooseMovementUserInput(target_card, position, destination_card)
        engine_playing_player = self.engine_player(player)
        self.engine_game.proceed(engine_playing_player, movement_input)

        self.event_handler.send_events()

    @ActionHandler.action()
    def send_game_chat_message(self, player, message):
        send_game_message(message, player, self)

    @staticmethod
    def identify_enum(element_name, enum, exception):
        try:
            return [value for element, value in enum.__members__.items() if element.lower() == element_name.lower()][0]
        except IndexError:
            raise TerminalEventError(exception(element_name))

    def identify_player(self, player_id):
        try:
            return [p for p in self.players if p.id == player_id][0]
        except IndexError:
            raise TerminalEventError(no_such_player_event(player_id))

    @classmethod
    def identify_card_kind(cls, card_name):
        return cls.identify_enum(card_name, CardName, not_a_card_name_event)

    @classmethod
    def identify_position(cls, position):
        return cls.identify_enum(position, Position, not_a_position_name_event)

    @classmethod
    def identify_resolution_choice(cls, resolution_choice):
        return cls.identify_enum(resolution_choice, RevealChoice, not_a_resolution_choice_name_event)

    def identify_card(self, card_id):
        if not self.started:
            raise TerminalEventError(game_has_not_started_event(self))
        try:
            return self.cards[card_id]
        except KeyError:
            raise TerminalEventError(no_such_card_event(card_id))

    def as_dict(self):
        return {
            "name": self.name,
            "leaderId": self.leader.id if self.leader else None,
            "players": [player.id for player in self.players],
            "minPlayers": Game.MINIMUM_PLAYER_NUMBER,
            "maxPlayers": Game.MAXIMUM_PLAYER_NUMBER
        }

    @staticmethod
    def get_handler(server, params):
        return server.lobby.find_game(params["game_name"])

    def send_logged_player(self, event, other_than=None):
        for player in self.players:
            if player.logged and player != other_than:
                player.send(event)
