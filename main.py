import asyncio
import errno
import logging
import os

from src.server.server import Server
from datetime import datetime
from logging.handlers import TimedRotatingFileHandler

LOGS_FILE = 'logs'

try:
    os.makedirs(LOGS_FILE)
except OSError as e:
    if e.errno != errno.EEXIST:
        raise

file_handler = TimedRotatingFileHandler(f"{LOGS_FILE}/{datetime.now().strftime('%Y-%m-%d_%Hh%M')}.log", when="midnight")
filtered_out = ["data_received", "opcode=9,", "opcode=10,", "server - received solicited pong"]

logger = logging.getLogger("websockets")
logger.setLevel(logging.DEBUG)
logger.addHandler(file_handler)
logger.addHandler(logging.StreamHandler())

for handler in logger.handlers:
    handler.setFormatter(logging.Formatter("[%(asctime)s] %(message)s", datefmt="%Y-%m-%d %H:%M:%S"))

logging.getLogger("websockets.protocol").addFilter(lambda r: not any(f in r.getMessage() for f in filtered_out))

if __name__ == '__main__':
    event_loop = asyncio.get_event_loop()
    port = os.environ.get("ORIFLAMME_PORT", 80)
    logger.info(f"Start listening on port {port}")
    game_server = Server(port, event_loop)
    game_server.start()
    event_loop.run_forever()
